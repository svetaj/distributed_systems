#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

char * int2bin(int i)
{
    size_t bits = sizeof(int) * CHAR_BIT;

    char * str = malloc(bits + 1);
    if(!str) return NULL;
    str[bits] = 0;

    // type punning because signed shift is implementation-defined
    unsigned u = *(unsigned *)&i;
    for(; bits--; u >>= 1)
    	str[bits] = u & 1 ? '1' : '0';

    return str;
}
main()
{
    unsigned int m = 32, h1, h2, h3, x, i;
    unsigned int input[3] = {2013, 2010, 2007};
    unsigned int input1[6] = {2013, 2010, 2007, 2004, 2001, 1998};
    unsigned int input2[6] = {2010, 2013, 2007, 2004};
    unsigned int input3[6] = {2010, 2013, 2007, 2004};

    printf("=========== test h1(100) = 16 ================\n");
    x = 100;
    h1 = ((x*x +x*x*x)*1);
    printf("x=%u h1=%d\n", x, h1);

    printf("=========== Q36 ================\n");
    for (i = 0; i< 3; i++) {
       x = input[i];
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
       printf("x=%u h3=%d\n", x, h3);
    }


    printf("=========== Q37 ================\n");
    for (i = 0; i< 6; i++) {
       x = input1[i];
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
       printf("x=%u h3=%d\n", x, h3);
    }

    printf("=========== Q38 ================\n");
    for (i = 0; i< 4; i++) {
       x = input1[i] % m;
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
       printf("x=%u h3=%d\n", x, h3);
    }

    printf("------------------------------------\n");

       x = 2004;
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
       printf("x=%u h3=%d\n", x, h3);


    printf("=========== Q39 ================\n");
    for (i = 0; i< 4; i++) {
       x = input2[i] % m;
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
       printf("x=%u h3=%d\n", x, h3);
    }

    printf("------------------------------------\n");

       x = 3200;
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
       printf("x=%u h3=%d\n", x, h3);



}

/*
=========== test h1(100) = 16 ================
x=100 h1=16
=========== Q37 ================
x=29 h1=14
x=29 h2=28
x=29 h3=10
x=26 h1=12
x=26 h2=24
x=26 h3=4
x=23 h1=24
x=23 h2=16
x=23 h3=8

4,8,10,12,14,16,24,28






=========== Q38 ================
x=29 h1=14
x=29 h2=28   <-------------
x=29 h3=10
x=26 h1=12
x=26 h2=24
x=26 h3=4
x=23 h1=24
x=23 h2=16
x=23 h3=8
x=0 h1=0
x=0 h2=0
x=0 h3=0
x=3 h1=4
x=3 h2=8
x=3 h3=12
x=30 h1=28
x=30 h2=24
x=30 h3=20
*/


