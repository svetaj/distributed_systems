/**********************************
 * FILE NAME: MP1Node.cpp
 *
 * DESCRIPTION: Membership protocol run by this Node.
 * 				Definition of MP1Node class functions.
 **********************************/

#include "MP1Node.h"

/*
 * Note: You can change/add any functions in MP1Node.{h,cpp}
 */

/**
 * Overloaded Constructor of the MP1Node class
 * You can add new members to the class if you think it
 * is necessary for your logic to work
 */
MP1Node::MP1Node(Member *member, Params *params, EmulNet *emul, Log *log, Address *address) {
	for( int i = 0; i < 6; i++ ) {
		NULLADDR[i] = 0;
	}
	this->memberNode = member;
	this->emulNet = emul;
	this->log = log;
	this->par = params;
	this->memberNode->addr = *address;
}

/**
 * Destructor of the MP1Node class
 */
MP1Node::~MP1Node() {}

/**
 * FUNCTION NAME: recvLoop
 *
 * DESCRIPTION: This function receives message from the network and pushes into the queue
 * 				This function is called by a node to receive messages currently waiting for it
 */
int MP1Node::recvLoop() {
    if ( memberNode->bFailed ) {
    	return false;
    }
    else {
    	return emulNet->ENrecv(&(memberNode->addr), enqueueWrapper, NULL, 1, &(memberNode->mp1q));
    }
}

/**
 * FUNCTION NAME: enqueueWrapper
 *
 * DESCRIPTION: Enqueue the message from Emulnet into the queue
 */
int MP1Node::enqueueWrapper(void *env, char *buff, int size) {
	Queue q;
	return q.enqueue((queue<q_elt> *)env, (void *)buff, size);
}

/**
 * FUNCTION NAME: nodeStart
 *
 * DESCRIPTION: This function bootstraps the node
 * 				All initializations routines for a member.
 * 				Called by the application layer.
 */
void MP1Node::nodeStart(char *servaddrstr, short servport) {
    Address *joinaddr;
    joinaddr = getJoinAddress();

    // Self booting routines
    if( initThisNode(joinaddr) == -1 ) {
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "init_thisnode failed. Exit.");
#endif
        exit(1);
    }

    if( !introduceSelfToGroup(joinaddr) ) {
        finishUpThisNode();
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Unable to join self to group. Exiting.");
#endif
        exit(1);
    }

    return;
}

/**
 * FUNCTION NAME: initThisNode
 *
 * DESCRIPTION: Find out who I am and start up
 */
int MP1Node::initThisNode(Address *joinaddr) {
	/*
	 * This function is partially implemented and may require changes
	 */
	int id = *(int*)(&memberNode->addr.addr);
	int port = *(short*)(&memberNode->addr.addr[4]);

	memberNode->bFailed = false;
	memberNode->inited = true;
	memberNode->inGroup = false;
    // node is up!
	memberNode->nnb = 0;
	memberNode->heartbeat = 0;
	memberNode->pingCounter = TFAIL;
	memberNode->timeOutCounter = -1;
        initMemberListTable(memberNode);
        updateLocalHeartbeat();
    return 0;
}

/**
 * FUNCTION NAME: introduceSelfToGroup
 *
 * DESCRIPTION: Join the distributed system
 */
int MP1Node::introduceSelfToGroup(Address *joinaddr) {
	MessageHdr *msg;
#ifdef DEBUGLOG
    static char s[1024];
#endif

    if ( 0 == memcmp((char *)&(memberNode->addr.addr), (char *)&(joinaddr->addr), sizeof(memberNode->addr.addr))) {
        // I am the group booter (first process to join the group). Boot up the group
#ifdef DEBUGLOG
        log->LOG(&memberNode->addr, "Starting up group...");
        log->logNodeAdd(&(memberNode->addr), joinaddr);
#endif
        memberNode->inGroup = true;
    }
    else {
        size_t msgsize = sizeof(MessageHdr) + sizeof(joinaddr->addr) + sizeof(long) + 1;
        msg = (MessageHdr *) malloc(msgsize * sizeof(char));

        // create JOINREQ message: format of data is {struct Address myaddr}
        msg->msgType = JOINREQ;
        memcpy((char *)(msg+1), &memberNode->addr.addr, sizeof(memberNode->addr.addr));
        memcpy((char *)(msg+1) + 1 + sizeof(memberNode->addr.addr), &memberNode->heartbeat, sizeof(long));

#ifdef DEBUGLOG
        sprintf(s, "Trying to join...");
        log->LOG(&memberNode->addr, s);
#endif

        // send JOINREQ message to introducer member
        emulNet->ENsend(&memberNode->addr, joinaddr, (char *)msg, msgsize);

        free(msg);
    }

    return 1;

}


/**
 * FUNCTION NAME: acceptedByTheGroup
 *
 * DESCRIPTION: Join the distributed system
 */
int MP1Node::acceptedByTheGroup(Address *newaddr) {
	MessageHdr *msg;
#ifdef DEBUGLOG
    static char s[1024];
#endif

        size_t msgsize = sizeof(MessageHdr) + sizeof(newaddr->addr) + sizeof(long) + 1;
        msg = (MessageHdr *) malloc(msgsize * sizeof(char));

        // create JOINREQ message: format of data is {struct Address myaddr}
        msg->msgType = JOINREP;
        memcpy((char *)(msg+1), &memberNode->addr.addr, sizeof(memberNode->addr.addr));
        memcpy((char *)(msg+1) + 1 + sizeof(memberNode->addr.addr), &memberNode->heartbeat, sizeof(long));

        // send JOINREP message to introducer member
        Address *jp = getJoinAddress();
        emulNet->ENsend(jp, newaddr, (char *)msg, msgsize);

        free(msg);

    return 1;

}

/**
 * FUNCTION NAME: finishUpThisNode
 *
 * DESCRIPTION: Wind up this node and clean up state
 */
int MP1Node::finishUpThisNode(){
   /*
    * Your code goes here
    */
     memberNode->memberList.clear();
/*     free(emulNet);
     free(log);
     free(par);
     free(memberNode);  */
     return 0;
}


/**
 * FUNCTION NAME: gossipToAll
 *
 * DESCRIPTION: Gossip membership table
 */
void MP1Node::updateLocalHeartbeat() {
    int myid = 0;
    short myport = 0;
    memcpy(&myid, &(memberNode->addr.addr[0]), sizeof(int));
    memcpy(&myport, &(memberNode->addr.addr[4]), sizeof(short));
    int nx = memberNode->memberList.size(); 
    //cout <<  "SIZE ------------------ nxi=" << nx << " myid=" << myid << endl; 
    for (int i = 0; i < nx; i++) {
         int idx = memberNode->memberList[i].getid();
         short portx = memberNode->memberList[i].getport();
         if (myid == idx && myport == portx) {
             memberNode->heartbeat++;
             memberNode->memberList[i].setheartbeat(memberNode->heartbeat);
             long tsp = par-> getcurrtime();
             memberNode->memberList[i].settimestamp(tsp);
             break;
         }
    }
}

/**
 * FUNCTION NAME: gossipToAll
 *
 * DESCRIPTION: Gossip membership table
 */
void  MP1Node::gossipToAll() {
#ifdef DEBUGLOG
    static char s[1024];
#endif
    int nx = memberNode->memberList.size(); 
    int nx1;
    nx1 = 3;
    if (nx < 3) nx1 = nx;
    int sentnodes[3] = {-1,-1,-1};
    int myid = 0;
    short myport = 0;
    memcpy(&myid, &(memberNode->addr.addr[0]), sizeof(int));
    memcpy(&myport, &(memberNode->addr.addr[4]), sizeof(short));

    int myEntry = 0;
    for (int i = 0; i < nx1; i++) {
         int j = (rand() % nx);
         if (j == sentnodes[0] || j == sentnodes[1] || j == sentnodes[2]) continue;
         sentnodes[i] = j;
         int idx = memberNode->memberList[j].getid();
         if (idx < 0) continue;  
         short portx = memberNode->memberList[j].getport();
         long hb =  memberNode->memberList[j].getheartbeat();
         if (hb < 0) continue;  
         if (myid == idx && myport == portx) continue;
         gossipToMember(idx, portx);
    }
}

/**
 * FUNCTION NAME: gossipToMember
 *
 * DESCRIPTION: Gossip membership table
 */
int MP1Node::gossipToMember(int idx, short portx) {
	MessageHdr *msg;

    if (idx < 0) return 1;   
    Address *xaddr = computeAddress(idx, portx);
    int nx = memberNode->memberList.size(); 
    int listsz = nx*sizeof(MemberListEntry);

    size_t msgsize = sizeof(MessageHdr) + sizeof(memberNode->addr.addr) + sizeof(long) + listsz +1 ;
    msg = (MessageHdr *) malloc(msgsize * sizeof(char));

    // create GOSSIP message: format of data is {struct Address myaddr}
    msg->msgType = GOSSIP;
    memcpy((char *)(msg+1), &memberNode->addr.addr, sizeof(memberNode->addr.addr));
    memcpy((char *)(msg+1) + 1 + sizeof(memberNode->addr.addr), &(memberNode->heartbeat), sizeof(long)); 
    memcpy((char *)(msg+1) + 1 + sizeof(memberNode->addr.addr) + sizeof(long), &(memberNode->memberList), listsz);

    // send GOSSIP message to introducer member
    emulNet->ENsend(&memberNode->addr, xaddr, (char *)msg, msgsize);

    free(msg);

    return 1;

}
/**
 * FUNCTION NAME: nodeLoop
 *
 * DESCRIPTION: Executed periodically at each member
 * 				Check your messages in queue and perform membership protocol duties
 */
void MP1Node::nodeLoop() {
    if (memberNode->bFailed) {
    	return;
    }

    // Check my messages
    checkMessages();

    // Wait until you're in the group...
    if( !memberNode->inGroup ) {
    	return;
    }

    // ...then jump in and share your responsibilites!
    nodeLoopOps();

    return;
}

/**
 * FUNCTION NAME: checkMessages
 *
 * DESCRIPTION: Check messages in the queue and call the respective message handler
 */
void MP1Node::checkMessages() {
    void *ptr;
    int size;

    // Pop waiting messages from memberNode's mp1q
    while ( !memberNode->mp1q.empty() ) {
    	ptr = memberNode->mp1q.front().elt;
    	size = memberNode->mp1q.front().size;
    	memberNode->mp1q.pop();
    	recvCallBack((void *)memberNode, (char *)ptr, size);
    }
    return;
}

/**
 * FUNCTION NAME: recvCallBack
 *
 * DESCRIPTION: Message handler for different message types
 */
bool MP1Node::recvCallBack(void *env, char *data, int size ) {
	/*
	 * Your code goes here
	 */
        /* �size_t msgsize = sizeof(MessageHdr) + sizeof(memberNode->addr.addr) +1 +  sizeof(long) +listsz ; */
        char s[100];
        //Check msg type
        MessageHdr *hdr = reinterpret_cast<MessageHdr*>(data);
        MsgTypes *mm = reinterpret_cast<MsgTypes*>(hdr);
        // Get sender addr
        Address *addr = reinterpret_cast<Address*>(hdr+1);
        // Get sender hbeat
        long hbeat = *( (long *) (data + sizeof(MessageHdr) + sizeof(memberNode->addr.addr) +1)); 
        Address *jp = getJoinAddress();
        if ( *mm == JOINREQ && 0 == memcmp((char *)&(memberNode->addr.addr), (char *)&(jp->addr), sizeof(memberNode->addr.addr))) {
            acceptedByTheGroup(addr);
#ifdef DEBUGLOG
            log->logNodeAdd(jp, addr);
#endif
            int id = 0;
            short port;
            long tsp = par-> getcurrtime();
            memcpy(&id, &(addr->addr[0]), sizeof(int));
            memcpy(&port, &(addr->addr[4]), sizeof(short));
            insertMemberListTable(id, port, hbeat, tsp);
        }

        if ( *mm == JOINREP && 0 != memcmp((char *)&(memberNode->addr.addr), (char *)&(jp->addr), sizeof(memberNode->addr.addr))) {
            int id = 0;
            short port;
            long tsp = par-> getcurrtime();
            memcpy(&id, &(addr->addr[0]), sizeof(int));
            memcpy(&port, &(addr->addr[4]), sizeof(short));
	    memberNode->inGroup = true;
            insertMemberListTable(id, port, hbeat, tsp);
#ifdef DEBUGLOG
            log->logNodeAdd(&(memberNode->addr), &(memberNode->addr));
            log->logNodeAdd(&(memberNode->addr), jp);
#endif
        }
        if ( *mm == GOSSIP) {
            vector<MemberListEntry> *newml;
            newml = reinterpret_cast<vector<MemberListEntry>*>((vector<MemberListEntry> *) (data + sizeof(MessageHdr) + sizeof(memberNode->addr.addr) +1+sizeof(long)));
            updateMemberListTable(newml);   
            updateLocalHeartbeat();
        }
        return true;
}


/**
 * FUNCTION NAME: printMemberListTable
 *
 * DESCRIPTION: Print membership list 
 */

void MP1Node::printMemberListTable() {

     int xid, currid;
     short xport, currport;
     long xbit, xtmsp;
     memcpy(&currid, &(memberNode->addr.addr[0]), sizeof(int));
     memcpy(&currport, &(memberNode->addr.addr[4]), sizeof(short));
     int currnx = memberNode->memberList.size();
     long currtmsp = par-> getcurrtime();
     for (int i = 0; i < currnx; i++) {
          xid   = memberNode->memberList[i].getid();
          xport = memberNode->memberList[i].getport(); 
          xbit = memberNode->memberList[i].getheartbeat();
          xtmsp = memberNode->memberList[i].gettimestamp();
          cout << "MLT["<<currid<<"]["<<i<<"]"<<"["<<xid<<","<<xport<<","<<xbit<<","<<xtmsp<<"]"<<endl;
     }
}



/**
 * FUNCTION NAME: checkMemberListTable
 *
 * DESCRIPTION: Check membership list 
 */
void MP1Node::checkMemberListTable() {
     int xid, currid;
     short xport, currport;
     long xbit, xtmsp;
     memcpy(&currid, &(memberNode->addr.addr[0]), sizeof(int));
     memcpy(&currport, &(memberNode->addr.addr[4]), sizeof(short));
     int currnx = memberNode->memberList.size();
     long currtmsp = par-> getcurrtime(); 
     for (int i = 0; i < currnx; i++) {
          xid   = memberNode->memberList[i].getid();
          xport = memberNode->memberList[i].getport();
          if (currid == xid && currport == xport) continue;  /* skip self */
          if (xid < 0) continue;   
          xbit = memberNode->memberList[i].getheartbeat();
          xtmsp = memberNode->memberList[i].gettimestamp();
          Address *xaddr = computeAddress(xid, xport);
          if ( currtmsp > xtmsp + TREMOVE && xbit < 0) {   
#ifdef DEBUGLOG
              log->logNodeRemove(&(memberNode->addr), xaddr);
#endif
              memberNode->memberList[i].setid(-1*xid);         /* flag as definitely removed */
              continue;
          }
          if (currtmsp > xtmsp + TFAIL) { 
               memberNode->memberList[i].setheartbeat(-1*xbit); 
          }
     }
}



/**
 * FUNCTION NAME: updateMemberListTable
 *
 * DESCRIPTION: Update membership list using table receiver from other mode
 */
void MP1Node::updateMemberListTable(vector<MemberListEntry> *mlt) {
     char s[100];
     int oldid = -1, newid, currid;
     short oldport, newport, currport;
     long currtmsp = par-> getcurrtime(), newtmsp, oldtmsp;
     long oldhbit, newhbit, currhbit;
     memcpy(&currid, &(memberNode->addr.addr[0]), sizeof(int));
     memcpy(&currport, &(memberNode->addr.addr[4]), sizeof(short));
     int currnx = memberNode->memberList.size();
     int newnx = mlt->size();
     int indx = -1;
     for (int i = 0; i < newnx; i++) {
          MemberListEntry newmle = mlt->at(i);
          newid   = newmle.getid();
          newport = newmle.getport();
          newhbit = newmle.getheartbeat();
          newtmsp = newmle.gettimestamp();
          if (newid < 0) continue;
          if (newhbit < 0) continue;
          Address *newaddr = computeAddress(newid, newport);
          if (currid == newid && currport == newport) continue;  /* skip self */
          indx = -1;
          for (int j = 0; j < currnx; j++) {
               if (memberNode->memberList[j].getid() == newid && memberNode->memberList[j].getport() == newport) indx = j;
          }
          if (indx >= 0) {   /* MAIN GOSSIP PROTOCOL ACTION, CHECK HEARTHBEAT */
                oldhbit = memberNode->memberList[indx].getheartbeat();
                if (newhbit > 0) {
                    oldtmsp = memberNode->memberList[indx].gettimestamp();
                    oldid = memberNode->memberList[indx].getid();
                    if (newhbit > oldhbit ) {
                         memberNode->memberList[indx].setheartbeat(newhbit);
                         memberNode->memberList[indx].settimestamp(currtmsp);
                    }
                }
          }
          else {
                bool exists = false;
                for (int j = 0; j < currnx; j++) {
                     if (abs(memberNode->memberList[j].getid()) == newid && memberNode->memberList[j].getport() == newport) {
                          exists = true;
                          break;
                     }
                }
                if (!exists) {  
                     Address *newaddr = computeAddress(newid, newport);
                     insertMemberListTable(newid, newport, newhbit, newtmsp);
#ifdef DEBUGLOG
                     log->logNodeAdd(&(memberNode->addr), newaddr);
#endif
                }  
          }
     }
}

/**
 * FUNCTION NAME: nodeLoopOps
 *
 * DESCRIPTION: Check if any node hasn't responded within a timeout period and then delete
 * 				the nodes
 * 				Propagate your membership list
 */
void MP1Node::nodeLoopOps() {
	/*
	 * Your code goes here
	 */

    int idx = 0;
    memcpy(&idx, &(memberNode->addr.addr[0]), sizeof(int));
    int idx1 = idx % 10;
    long tsp = par-> getcurrtime();
    updateLocalHeartbeat();
    if (tsp % 10 == idx1) {  
        gossipToAll();  
    } 
    if (tsp > 300 && ((tsp % 10) == ((idx + 3) % 10) )) checkMemberListTable();
    //if (tsp == 299) printMemberListTable();
    if (tsp == 326) printMemberListTable();
    return;
}

/**
 * FUNCTION NAME: isNullAddress
 *
 * DESCRIPTION: Function checks if the address is NULL
 */
int MP1Node::isNullAddress(Address *addr) {
	return (memcmp(addr->addr, NULLADDR, 6) == 0 ? 1 : 0);
}

/**
 * FUNCTION NAME: getJoinAddress
 *
 * DESCRIPTION: Returns the Address of the coordinator
 */
Address *MP1Node::getJoinAddress() {
    Address *joinaddr = new Address();

    memset(joinaddr, 0, sizeof(Address));
    *(int *)(&joinaddr->addr[0]) = 1;
    *(short *)(&joinaddr->addr[4]) = 0;

    return joinaddr;
}

/**
 * FUNCTION NAME: computeAddress
 *
 * DESCRIPTION: Returns the Address for given ID
 */

Address *MP1Node::computeAddress(int id, short port) {
    Address *xaddr = new Address();
 
    memset(xaddr, 0, sizeof(Address));
    *(int *)(&xaddr->addr[0]) = id;
    *(short *)(&xaddr->addr[4]) = port;

    return xaddr;
}

/**
 * FUNCTION NAME: initMemberListTable
 *
 * DESCRIPTION: Initialize the membership list
 */
void MP1Node::initMemberListTable(Member *memberNode) {
	memberNode->memberList.clear();
        int id = 0;
        short port;
        long hbt = memberNode->heartbeat; 
        long tsp = par-> getcurrtime();
        memcpy(&id, &(memberNode->addr.addr[0]), sizeof(int));
        memcpy(&port, &(memberNode->addr.addr[4]), sizeof(short));
        insertMemberListTable(id, port, hbt, tsp);
}

/**
 * FUNCTION NAME: insertMemberListTable
 *
 * DESCRIPTION: Insert into the membership list
 */
void MP1Node::insertMemberListTable(int id1, short port1, long hbt1, long tsp1) {
        MemberListEntry *mle =  new MemberListEntry(); 
        mle->id = id1;
        mle->port = port1;
        mle->heartbeat = hbt1;
        mle->timestamp = tsp1; 
        memberNode->memberList.push_back(*mle);  
        int mlsz = memberNode->memberList.size();
}

/**
 * FUNCTION NAME: printAddress
 *
 * DESCRIPTION: Print the Address
 */
void MP1Node::printAddress(Address *addr)
{
    printf("%d.%d.%d.%d:%d \n",  addr->addr[0],addr->addr[1],addr->addr[2],
                                                       addr->addr[3], *(short*)&addr->addr[4]) ;    
}

