#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

main()
{
    unsigned int m = 64, h1, h2, h3, x, i;
    unsigned int input[4] = {1975, 1985, 1995, 2005};

    printf("=========== Q36 ================\n");
    for (i = 0; i< 4; i++) {
       x = input[i];
       h1 = (x*1) % m;
       h2 = (x*2) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
    }

    printf("------------------------------------\n");

       x = 2015;
       h1 = (x*1) % m;
       h2 = (x*2) % m;
       printf("x=%u h1=%d\n", x, h1);
       printf("x=%u h2=%d\n", x, h2);
}