#include <stdio.h>

main()
{
    int m = 32, h1, h2, h3, x, i;
    int input[3] = {2013, 2010, 2007};

    for (i = 0; i< 3; i++) {
       x = input[i];
       h1 = ((x*x +x*x*x)*1) % m;
       h2 = ((x*x +x*x*x)*2) % m;
       h3 = ((x*x +x*x*x)*3) % m;
       printf("x=%d h1=%d\n", x, h1);
       printf("x=%d h2=%d\n", x, h2);
       printf("x=%d h3=%d\n", x, h3);
    }
}